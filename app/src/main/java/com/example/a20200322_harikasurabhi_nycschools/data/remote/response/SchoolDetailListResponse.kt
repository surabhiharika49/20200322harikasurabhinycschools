package com.example.a20200322_harikasurabhi_nycschools.data.remote.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SchoolDetailListResponse(
    @Expose
    @SerializedName("school_name")
    val school_name: String,

    @Expose
    @SerializedName("num_of_sat_test_takers")
    val num_of_sat_test_takers: String,

    @Expose
    @SerializedName("sat_critical_reading_avg_score")
    val sat_critical_reading_avg_score: String,

    @Expose
    @SerializedName("sat_math_avg_score")
    val sat_math_avg_score: String,

    @Expose
    @SerializedName("sat_writing_avg_score")
    val sat_writing_avg_score: String
    )