package com.example.a20200322_harikasurabhi_nycschools.data.remote.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SchoolListResponse(
    @Expose
    @SerializedName("dbn")
    val dbn: String,

    @Expose
    @SerializedName("school_name")
    val school_name: String,

    @Expose
    @SerializedName("boro")
    val boro: String,

    @Expose
    @SerializedName("academicopportunities1")
    val academicopportunities1: String,

    @Expose
    @SerializedName("academicopportunities2")
    val academicopportunities2: String,

    @Expose
    @SerializedName("academicopportunities3")
    val academicopportunities3: String,

    @Expose
    @SerializedName("academicopportunities4")
    val academicopportunities4: String,

    @Expose
    @SerializedName("academicopportunities5")
    val academicopportunities5: String,

    @Expose
    @SerializedName("ell_programs")
    val ell_programs: String,

    @Expose
    @SerializedName("language_classes")
    val language_classes: String,

    @Expose
    @SerializedName("neighborhood")
    val neighborhood: String,

    @Expose
    @SerializedName("shared_space")
    val shared_space: String,

    @Expose
    @SerializedName("campus_name")
    val campus_name: String,

    @Expose
    @SerializedName("building_code")
    val building_code: String,

    @Expose
    @SerializedName("location")
    val location: String,

    @Expose
    @SerializedName("fax_number")
    val fax_number: String,

    @Expose
    @SerializedName("school_email")
    val school_email: String,

    @Expose
    @SerializedName("website")
    val website: String,

    @Expose
    @SerializedName("subway")
    val subway: String,

    @Expose
    @SerializedName("bus")
    val bus: String,

    @Expose
    @SerializedName("grades2018")
    val grades2018: String,

    @Expose
    @SerializedName("finalgrades")
    val finalgrades: String,

    @Expose
    @SerializedName("total_students")
    val total_students: String,

    @Expose
    @SerializedName("start_time")
    val start_time: String,

    @Expose
    @SerializedName("end_time")
    val end_time: String,

    @Expose
    @SerializedName("extracurricular_activities")
    val extracurricular_activities: String,

    @Expose
    @SerializedName("psal_sports_boys")
    val psal_sports_boys: String,

    @Expose
    @SerializedName("psal_sports_girls")
    val psal_sports_girls: String,

    @Expose
    @SerializedName("psal_sports_coed")
    val psal_sports_coed: String,

    @Expose
    @SerializedName("school_sports")
    val school_sports: String,

    @Expose
    @SerializedName("graduation_rate")
    val graduation_rate: String,

    @Expose
    @SerializedName("attendance_rate")
    val attendance_rate: String,

    @Expose
    @SerializedName("pct_stu_enough_variety")
    val pct_stu_enough_variety: String,

    @Expose
    @SerializedName("college_career_rate")
    val college_career_rate: String,

    @Expose
    @SerializedName("pct_stu_safe")
    val pct_stu_safe: String,

    @Expose
    @SerializedName("school_accessibility_description")
    val school_accessibility_description: String,

    @Expose
    @SerializedName("prgdesc1")
    val prgdesc1: String,

    @Expose
    @SerializedName("prgdesc2")
    val prgdesc2: String
)