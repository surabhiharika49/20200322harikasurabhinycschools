package com.example.a20200322_harikasurabhi_nycschools.data.remote

import com.example.a20200322_harikasurabhi_nycschools.data.Networking
import com.example.a20200322_harikasurabhi_nycschools.data.remote.response.SchoolDetailListResponse
import com.example.a20200322_harikasurabhi_nycschools.data.remote.response.SchoolListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import javax.inject.Singleton

@Singleton
interface NetworkService {

    @GET(EndPoints.SCHOOL_LIST)
    fun getSchoolList(
        @Header(Networking.HEADER_API_KEY) apiKey: String = Networking.API_KEY
    ): Single<List<SchoolListResponse>>

    @GET(EndPoints.SCHOOL_DETAIL_LIST)
    fun getSchoolDetailList(
        @Header(Networking.HEADER_API_KEY) apiKey: String = Networking.API_KEY,
        @Query("dbn") dbn: String
    ): Single<List<SchoolDetailListResponse>>

}