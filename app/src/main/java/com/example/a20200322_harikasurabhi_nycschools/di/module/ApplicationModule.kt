package com.example.a20200322_harikasurabhi_nycschools.di.module

import android.content.Context
import com.example.a20200322_harikasurabhi_nycschools.BuildConfig
import com.example.a20200322_harikasurabhi_nycschools.MyApplication
import com.example.a20200322_harikasurabhi_nycschools.data.Networking
import com.example.a20200322_harikasurabhi_nycschools.data.remote.NetworkService

import com.example.a20200322_harikasurabhi_nycschools.di.ApplicationContext

import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: MyApplication) {

    @ApplicationContext
    @Provides
    fun provideContext(): Context = application

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @Singleton
    fun provideNetworkService(): NetworkService =
        Networking.create(
            BuildConfig.API_KEY,
            BuildConfig.BASE_URL,
            application.cacheDir,
            10 * 1024 * 1024 // 10MB
        )
}
