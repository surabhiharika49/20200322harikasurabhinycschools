package com.example.a20200322_harikasurabhi_nycschools.di.module

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import com.example.a20200322_harikasurabhi_nycschools.data.remote.NetworkService
import com.example.a20200322_harikasurabhi_nycschools.di.ActivityContext
import com.example.a20200322_harikasurabhi_nycschools.ui.base.BaseActivity
import com.example.a20200322_harikasurabhi_nycschools.ui.main.MainViewModel
import com.example.a20200322_harikasurabhi_nycschools.ui.utils.NetworkHelper
import com.example.a20200322_harikasurabhi_nycschools.ui.utils.ViewModelProviderFactory
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class ActivityModule(private val activity: BaseActivity<*>) {

    @ActivityContext
    @Provides
    fun provideContext(): Context = activity

    @Provides
    fun provideMainViewModel(
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        networkService: NetworkService
    ): MainViewModel = ViewModelProviders.of(
        activity, ViewModelProviderFactory(MainViewModel::class) {
        MainViewModel(compositeDisposable, networkHelper, networkService)
    }).get(MainViewModel::class.java)
}
