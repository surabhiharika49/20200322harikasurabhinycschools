package com.example.a20200322_harikasurabhi_nycschools.di.component

import android.content.Context
import com.example.a20200322_harikasurabhi_nycschools.MyApplication
import com.example.a20200322_harikasurabhi_nycschools.data.remote.NetworkService
import com.example.a20200322_harikasurabhi_nycschools.di.ApplicationContext
import com.example.a20200322_harikasurabhi_nycschools.di.module.ApplicationModule
import com.example.a20200322_harikasurabhi_nycschools.ui.utils.NetworkHelper
import dagger.Component
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(application: MyApplication)

    @ApplicationContext
    fun getContext(): Context

    fun getNetworkService(): NetworkService

    fun getNetworkHelper(): NetworkHelper

    fun getCompositeDisposable(): CompositeDisposable
}
