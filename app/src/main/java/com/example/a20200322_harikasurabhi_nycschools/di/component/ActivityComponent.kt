package com.example.a20200322_harikasurabhi_nycschools.di.component

import com.example.a20200322_harikasurabhi_nycschools.di.ActivityScope
import com.example.a20200322_harikasurabhi_nycschools.di.module.ActivityModule
import com.example.a20200322_harikasurabhi_nycschools.ui.main.MainActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [ApplicationComponent::class], modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(activity: MainActivity)
}
