package com.example.a20200322_harikasurabhi_nycschools.di.component

import com.example.a20200322_harikasurabhi_nycschools.di.FragmentScope
import com.example.a20200322_harikasurabhi_nycschools.ui.home.SchoolDetailListFragment
import com.example.a20200322_harikasurabhi_nycschools.ui.home.SchoolListFragment
import com.example.a20200322_harikasurabhi_nycschools.di.module.FragmentModule
import dagger.Component


@FragmentScope
@Component(dependencies = [ApplicationComponent::class], modules = [FragmentModule::class])
interface FragmentComponent {

    fun inject(fragment: SchoolListFragment)

    fun inject(fragment: SchoolDetailListFragment)
}

