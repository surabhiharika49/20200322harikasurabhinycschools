package com.example.a20200322_harikasurabhi_nycschools.di.module

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20200322_harikasurabhi_nycschools.data.remote.NetworkService
import com.example.a20200322_harikasurabhi_nycschools.di.ActivityContext
import com.example.a20200322_harikasurabhi_nycschools.ui.base.BaseFragment
import com.example.a20200322_harikasurabhi_nycschools.ui.home.SchoolDetailViewModel
import com.example.a20200322_harikasurabhi_nycschools.ui.home.SchoolListViewModel
import com.example.a20200322_harikasurabhi_nycschools.ui.utils.NetworkHelper
import com.example.a20200322_harikasurabhi_nycschools.ui.utils.ViewModelProviderFactory
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class FragmentModule(private val fragment: BaseFragment<*>) {

    @ActivityContext
    @Provides
    fun provideContext(): Context = fragment.context!!

    @Provides
    fun provideSchoolListViewModel(
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        networkService: NetworkService
    ): SchoolListViewModel = ViewModelProviders.of(
        fragment, ViewModelProviderFactory(SchoolListViewModel::class) {
        SchoolListViewModel(compositeDisposable, networkHelper, networkService)
    }).get(SchoolListViewModel::class.java)

    @Provides
    fun provideSchoolDetailViewModel(
        compositeDisposable: CompositeDisposable,
        networkHelper: NetworkHelper,
        networkService: NetworkService
    ): SchoolDetailViewModel = ViewModelProviders.of(
        fragment, ViewModelProviderFactory(SchoolDetailViewModel::class) {
        SchoolDetailViewModel(compositeDisposable, networkHelper, networkService)
    }).get(SchoolDetailViewModel::class.java)


    @Provides
    fun provideLinearLayoutManager() = LinearLayoutManager(fragment.context)

}
