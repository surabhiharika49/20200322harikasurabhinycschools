package com.example.a20200322_harikasurabhi_nycschools.ui.home

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import com.example.a20200322_harikasurabhi_nycschools.data.remote.NetworkService
import com.example.a20200322_harikasurabhi_nycschools.data.remote.response.SchoolListResponse
import com.example.a20200322_harikasurabhi_nycschools.ui.base.BaseViewModel
import com.example.a20200322_harikasurabhi_nycschools.ui.utils.NetworkHelper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SchoolListViewModel(
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val networkService: NetworkService
) : BaseViewModel(compositeDisposable, networkHelper) {

    var schoolListData = MutableLiveData<List<SchoolListResponse>>()

    var selectSchoolName = MutableLiveData<String>()

    var loading = ObservableBoolean(false)

    companion object {
        const val TAG = "HomeViewModel"
    }

    init {
        getSchoolData()
    }

    override fun onCreate() {
    }

    private fun getSchoolData() {
        compositeDisposable.add(
            networkService.getSchoolList()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { loading.set(true) }
                .subscribe({
                    schoolListData.postValue(it)
                }, {
                    Log.d(TAG, it.toString())
                })
        )
    }
}