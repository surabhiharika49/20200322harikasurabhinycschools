package com.example.a20200322_harikasurabhi_nycschools.ui.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a20200322_harikasurabhi_nycschools.ui.utils.NetworkHelper
import io.reactivex.disposables.CompositeDisposable

//Base viewmodel will extend android view model
//all common things like composite disposable, network helper required by all view models will be defined here
abstract class BaseViewModel(
    protected val compositeDisposable: CompositeDisposable,
    protected val networkHelper: NetworkHelper
) : ViewModel() {

    val messageStringId = MutableLiveData<Int>()

    val messageString = MutableLiveData<String>()

    protected fun checkInternetConnection() = networkHelper.isNetworkConnected()

    //check Network state error
    protected fun handleNetworkError(error: Throwable) {
        //implement later
    }

    override fun onCleared() {
        compositeDisposable.dispose() // any background operation, it will not call UI
        super.onCleared()
    }

    abstract fun onCreate()
}