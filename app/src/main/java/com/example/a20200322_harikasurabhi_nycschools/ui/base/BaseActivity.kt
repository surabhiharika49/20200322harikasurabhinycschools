package com.example.a20200322_harikasurabhi_nycschools.ui.base

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.a20200322_harikasurabhi_nycschools.MyApplication
import com.example.a20200322_harikasurabhi_nycschools.di.component.ActivityComponent
import com.example.a20200322_harikasurabhi_nycschools.di.component.DaggerActivityComponent
import com.example.a20200322_harikasurabhi_nycschools.di.module.ActivityModule
import javax.inject.Inject

abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {

    //here type of viewmodel depends, on the class which is baseviewmodel or which extends baseviewmodel
    @Inject
    lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies(buildActivityComponent())
        super.onCreate(savedInstanceState)
        setContentView(provideLayoutId())
        setUpObserver()
        setUpView(savedInstanceState)
        viewModel.onCreate()
    }

    protected open fun setUpObserver() {
        viewModel.messageString.observe(this, Observer {
            showMessage(it)
        })

        viewModel.messageStringId.observe(this, Observer {
            showMessageId(it)
        })
    }

    private fun buildActivityComponent() =
        DaggerActivityComponent
            .builder()
            .applicationComponent((application as MyApplication).applicationComponent)
            .activityModule(ActivityModule(this))
            .build()

    fun showMessage(message: String) =
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()


    fun showMessageId(@StringRes resId: Int) = showMessage(getString(resId))

    @LayoutRes
    protected abstract fun provideLayoutId(): Int

    protected abstract fun setUpView(savedInstanceState: Bundle?)

    protected abstract fun injectDependencies(activityComponent: ActivityComponent)

}