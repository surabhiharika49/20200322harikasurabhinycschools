package com.example.a20200322_harikasurabhi_nycschools.ui.main;

import android.os.Bundle;
import android.widget.TextView;

import androidx.lifecycle.Observer;

import com.example.a20200322_harikasurabhi_nycschools.R;
import com.example.a20200322_harikasurabhi_nycschools.di.component.ActivityComponent;
import com.example.a20200322_harikasurabhi_nycschools.ui.base.BaseActivity;
import com.example.a20200322_harikasurabhi_nycschools.ui.home.SchoolListFragment;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<MainViewModel> {

    @Inject
    MainViewModel mainViewModel;

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.header);
    }

    void addSchoolListFragment() {
        if (getSupportFragmentManager().findFragmentByTag(SchoolListFragment.Companion.getTAG()) == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, SchoolListFragment.Companion.newInstance(),
                            SchoolListFragment.Companion.getTAG())
                    .commit();
        }
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void setUpView(@Nullable Bundle savedInstanceState) {
        addSchoolListFragment();
    }

    @Override
    protected void injectDependencies(@NotNull ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void setUpObserver() {
        super.setUpObserver();
        viewModel.data.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                textView.setText(s);
            }
        });
    }

}
