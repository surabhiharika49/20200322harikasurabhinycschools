package com.example.a20200322_harikasurabhi_nycschools.ui.utils

import android.content.Context
import com.example.a20200322_harikasurabhi_nycschools.di.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkHelper @Inject constructor(
    @ApplicationContext private val context: Context) {

    // will check for network connectivity, implement later
    fun isNetworkConnected(): Boolean {
        return true
    }
}
