package com.example.a20200322_harikasurabhi_nycschools.ui.home.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20200322_harikasurabhi_nycschools.R;
import com.example.a20200322_harikasurabhi_nycschools.data.remote.response.SchoolListResponse;
import com.example.a20200322_harikasurabhi_nycschools.ui.home.SchoolListViewModel;

import java.util.List;

/**
 * If had more time, i would created base classes for  adapter and and view holder classes and added life cycle events
 * i.e onViewAttachedToWindow by injecting life cycle registry to viewholder
 */
public class SchoolDataAdapter extends RecyclerView.Adapter<SchoolDataAdapter.ViewHolder> {

    List<SchoolListResponse> listSchoolData;

    SchoolListViewModel schoolListViewModel;

    public SchoolDataAdapter(List<SchoolListResponse> listSchoolData, SchoolListViewModel schoolListViewModel) {
        this.listSchoolData = listSchoolData;
        this.schoolListViewModel = schoolListViewModel;
    }

    @NonNull
    @Override
    public SchoolDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //adding layout
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.school_data_adapter_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolDataAdapter.ViewHolder holder, int position) {
        //responsible for binding the data to layout
        final SchoolListResponse schoolListResponse = listSchoolData.get(position);
        final String dbn = schoolListResponse.getDbn();
        final String school_name = schoolListResponse.getSchool_name();
        holder.textViewdbn.setText(dbn);
        holder.textViewSchoolName.setText(school_name);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                schoolListViewModel.getSelectSchoolName().postValue(dbn);
            }
        });
    }

    @Override
    public int getItemCount() {
        //responsible for showing number of item in list
        return listSchoolData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewdbn, textViewSchoolName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewdbn = itemView.findViewById(R.id.dbn);
            textViewSchoolName = itemView.findViewById(R.id.school_name);
        }
    }
}
