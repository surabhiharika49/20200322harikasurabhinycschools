package com.example.a20200322_harikasurabhi_nycschools.ui.main

import androidx.lifecycle.MutableLiveData
import com.example.a20200322_harikasurabhi_nycschools.data.remote.NetworkService
import com.example.a20200322_harikasurabhi_nycschools.ui.base.BaseViewModel
import com.example.a20200322_harikasurabhi_nycschools.ui.utils.NetworkHelper
import io.reactivex.disposables.CompositeDisposable

class MainViewModel(
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val networkService: NetworkService
) : BaseViewModel(compositeDisposable, networkHelper) {

    @kotlin.jvm.JvmField
    val data = MutableLiveData<String>()

    override fun onCreate() {
        data.postValue("NYC Schools Data")
    }

    companion object {
        const val TAG = "MainViewModel"
    }

    fun onDestroy() {
        compositeDisposable.dispose()
    }
}