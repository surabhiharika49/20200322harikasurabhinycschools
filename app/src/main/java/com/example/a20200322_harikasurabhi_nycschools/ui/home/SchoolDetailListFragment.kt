package com.example.a20200322_harikasurabhi_nycschools.ui.home

import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.a20200322_harikasurabhi_nycschools.R
import com.example.a20200322_harikasurabhi_nycschools.di.component.FragmentComponent
import com.example.a20200322_harikasurabhi_nycschools.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_school_detail.num_of_sat_test_takers
import kotlinx.android.synthetic.main.fragment_school_detail.sat_critical_reading_avg_score
import kotlinx.android.synthetic.main.fragment_school_detail.sat_math_avg_score
import kotlinx.android.synthetic.main.fragment_school_detail.sat_writing_avg_score
import kotlinx.android.synthetic.main.fragment_school_detail.school_name

class SchoolDetailListFragment : BaseFragment<SchoolDetailViewModel>() {

    companion object {
        val TAG = "SchoolDetailListFragment"
    }

    override fun provideLayoutId(): Int = R.layout.fragment_school_detail

    override fun setUpView(view: View) {
    }

    override fun setUpObserver() {
        super.setUpObserver()
        val dbn = arguments?.get("dbn").toString()
        viewModel.getSchoolDetails(dbn)
        setData()
    }

    private fun setData() {
        viewModel.schoolDetailListData.observe(this, Observer {
            if (it.isNotEmpty()) {
                school_name.text = it.first().school_name
                num_of_sat_test_takers.text = it.first().num_of_sat_test_takers
                sat_critical_reading_avg_score.text = it.first().sat_critical_reading_avg_score
                sat_math_avg_score.text = it.first().sat_math_avg_score
                sat_writing_avg_score.text = it.first().sat_writing_avg_score
            } else
                Toast.makeText(context, "SAT results are empty for the selected school", Toast.LENGTH_SHORT).show()

        })
    }

    override fun injectDependencies(fragmentComponent: FragmentComponent) {
        fragmentComponent.inject(this)
    }

}