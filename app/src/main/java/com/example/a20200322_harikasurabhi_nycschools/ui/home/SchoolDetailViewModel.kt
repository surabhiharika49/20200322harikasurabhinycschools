package com.example.a20200322_harikasurabhi_nycschools.ui.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.a20200322_harikasurabhi_nycschools.data.remote.NetworkService
import com.example.a20200322_harikasurabhi_nycschools.data.remote.response.SchoolDetailListResponse
import com.example.a20200322_harikasurabhi_nycschools.ui.base.BaseViewModel
import com.example.a20200322_harikasurabhi_nycschools.ui.utils.NetworkHelper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SchoolDetailViewModel(
    compositeDisposable: CompositeDisposable,
    networkHelper: NetworkHelper,
    private val networkService: NetworkService
) : BaseViewModel(compositeDisposable, networkHelper) {

    var schoolDetailListData = MutableLiveData<List<SchoolDetailListResponse>>()

    override fun onCreate() {
    }

    internal fun getSchoolDetails(dbn: String) {
        compositeDisposable.add(
            networkService.getSchoolDetailList(dbn = dbn)
                .subscribeOn(Schedulers.io())
                .subscribe({
                    schoolDetailListData.postValue(it)
                }, {
                    Log.d(SchoolListViewModel.TAG, it.toString())
                })
        )
    }
}