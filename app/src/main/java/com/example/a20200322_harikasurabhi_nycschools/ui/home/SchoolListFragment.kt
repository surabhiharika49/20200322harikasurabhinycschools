package com.example.a20200322_harikasurabhi_nycschools.ui.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.a20200322_harikasurabhi_nycschools.R
import com.example.a20200322_harikasurabhi_nycschools.data.remote.response.SchoolListResponse
import com.example.a20200322_harikasurabhi_nycschools.di.component.FragmentComponent
import com.example.a20200322_harikasurabhi_nycschools.ui.base.BaseFragment
import com.example.a20200322_harikasurabhi_nycschools.ui.home.adapter.SchoolDataAdapter
import kotlinx.android.synthetic.main.fragment_school_list.recyclerView


class SchoolListFragment : BaseFragment<SchoolListViewModel>() {

    companion object {

        val TAG = "SchooListFragment"

        fun newInstance(): SchoolListFragment {
            val args = Bundle()
            val fragment = SchoolListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun provideLayoutId(): Int = R.layout.fragment_school_list

    override fun setUpView(view: View) {
    }

    override fun injectDependencies(fragmentComponent: FragmentComponent) {
        fragmentComponent.inject(this)
    }

    override fun setUpObserver() {
        super.setUpObserver()
        viewModel.schoolListData.observe(this,
            Observer {
                viewModel.loading.set(false)
                setUpRecyclerView(it)
            })

        viewModel.selectSchoolName.observe(this,
            Observer {
                val bundle = Bundle()
                if (it.isNotEmpty()) bundle.putString("dbn", it)
                val schoolDetailListFragment = SchoolDetailListFragment()
                schoolDetailListFragment.arguments = bundle
                val fm = fragmentManager
                val fragmentTransaction = fm!!.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, schoolDetailListFragment)
                    .commit()

            })

    }

    private fun setUpRecyclerView(it: List<SchoolListResponse>) {
        val schoolDataAdapter = SchoolDataAdapter(it, viewModel)
        val linearLayoutManager = LinearLayoutManager(this.context)
        linearLayoutManager.orientation = RecyclerView.VERTICAL
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = schoolDataAdapter
    }


}